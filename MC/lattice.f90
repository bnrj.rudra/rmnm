!File=lattice.f90
!Author=rudra
!Created=Fri 08 Feb 2019 12:08:24 IST
!Last Modified=Fri 08 Feb 2019 12:08:24 IST
Module mlatt
  Implicit None

  type::atom 
    character(len=2)::symb
    integer, dimension(3):: coor
    integer::spin
  end type atom

  integer:: i,j                                             ! dummy variable
  real(4)::prob                                             ! probability
  integer::sizen                                             ! size of lattice
  integer::spin                                             ! dummy for spin
  character(len=2),dimension(2)::aname                      ! dummy for name
  character(len=80)::ctmp                                   !
  type(atom),dimension(:,:), allocatable:: lat2d            ! 

  ! variable for command line arguments
  character(len=20):: arg
  integer:: stat

  !file handling
  character(80)::cmsg
  character(len=20)::opfile,inpfile


contains

  ! Get input from namelist
  subroutine get_input()

    implicit none
    namelist /latinp/sizen, aname, prob

    ! Reading input file from command line 
    call get_command_argument(1,arg, status=stat)
    if (stat /= 0) then                                     ! check if argument is supplied
      inpfile = "lattice.in"
      write(*,*) "Warning: Using default file ", trim(inpfile)
      ! call exit(1)
    else
      inpfile = arg
    End if
    write(*,*)"Getting Input from ", inpfile                ! we indeed got input file

    open(12, file=inpfile)
    read(12,NML=latinp, iostat=stat, iomsg=cmsg)
    ! write(*,*) stat
    if (stat /= 0) then
      write(*,*) "ERROR: ", trim(cmsg)
      call exit(1)
    End if
    close(12)

    ! Reading output file from command line 
    call get_command_argument(2,arg, status=stat)
    if (stat /= 0) then                                   ! check if argument is supplied
      opfile = "latt.xyz"
    else
      opfile = arg
    End if
    write(*,*)"Writting output to ", opfile               ! we indeed got input file

    allocate(lat2d(sizen,sizen))                            ! Allocate size of the lattice
  end subroutine get_input



  ! subroutine to generate the lattice
  subroutine  gen_lat()
    Implicit None
    open(12, file=opfile, iostat=stat, iomsg=cmsg)        ! Open the file to write
    write(12,*)sizen*sizen
    write(12,*)
    !$OMP PARALLEL DO
    do i = 1,sizen
      do j = 1,sizen
        ctmp = aname(1)
        spin =  1
        if (RAND()>prob) then
          ctmp = aname(2)
          spin = -1
        endif
        lat2d(i,j) =  atom(ctmp, (/2*i,2*j,0/), spin)
        write(12,*) lat2d(i,j)
        write(*,*) i,j,lat2d(i,j)%symb, lat2d(i,j)%coor
      End do
    End do
    !$OMP END PARALLEL DO
    close(12)
  end subroutine  gen_lat



  ! Flip a single spin
  subroutine  flip_spin()
    ! currently ignoring pbc
    Implicit None
    real(4), dimension(2):: harvest                                       ! random number
    integer,dimension(2):: ihar                                           ! integer random number 
    ! i = 3; j = 4        
    call random_number(harvest)
    do i = 1, size(harvest)
      ihar(i) = 1 + floor((sizen+1-1)*harvest(i))
    End do
    write(*,*)ihar
    i = ihar(1); j=ihar(2)
    write(*,*)"N ", i,j, lat2d(i,j)
    call gen_neighbour(ihar(1), ihar(2))

    lat2d(i,j)%spin = -1*lat2d(i,j)%spin
    if (lat2d(i,j)%symb=="Cl") then
     lat2d(i,j)%symb="Na"
    else
      lat2d(i,j)%symb="Cl"
    End if
    write(13,*)sizen*sizen
    write(*,*)
    !$OMP PARALLEL DO
    do i = 1,sizen
      do j = 1,sizen
        write(13,*)lat2d(i,j)
      End do
    End do
    !$OMP END PARALLEL DO
  end subroutine  flip_spin

  subroutine gen_neighbour(i,j)
    implicit none
    type(atom):: n_left, n_right, n_up,n_down
    integer,intent(in)::i,j
    integer::il,ip,ju,jd                       !i_left, i_right, j_up, j_down
    il = i - 1; ip = i + 1
    jd = j + 1; ju = j - 1
    ! Periodic Boundary Condition
    if (i == 1) il = sizen
    if (i == sizen) ip = 1
    if (j == 1) ju = sizen
    if (j == sizen) jd = 1

    n_up = lat2d(i, ju)
    n_down = lat2d(i, jd)
    n_left = lat2d(il,j)
    n_right = lat2d(ip,j)
    write(*,*)"nu", i,ju, n_up
    write(*,*)"nd", i,jd, n_down
    write(*,*)"nl", il,j, n_left
    write(*,*)"nr", ip,j, n_right
  End subroutine gen_neighbour

End Module mlatt


Program lattice2d
  use mlatt 
  implicit none

  call get_input()
  call gen_lat()
  write(*,*) "Before Flip"
  do i = 1,sizen
    do j = 1,sizen
      write(*,"(i2,','i2,':',2x, A2,2x,4(i2,2x))") i,j,lat2d(i,j)
    End do
  End do

  call flip_spin()
  ! write(*,*) "After Flip"
  ! do i = 1,sizen
    ! do j = 1,sizen
      ! write(*,"(i2,','i2,':',2x, A2,2x,4(i2,2x))") i,j, lat2d(i,j)
    ! End do
  ! End do


End Program lattice2d
