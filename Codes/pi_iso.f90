!File=pi_iso.f90
!Author=rudra
!Created=Fri 11 Jan 2019 12:39:19 IST
!Last Modified=Fri 11 Jan 2019 12:39:19 IST
Program pi_iso
  use iso_fortran_env
  implicit none
  real::pi
  real(real32):: pil
  real(real64)::piv
  real(real128)::piv2

  ! Define \pi as 4*arctan(1)
  pi =  4.0*atan(1.0)
  pil =  4.0_real32*atan(1.0_real32)
  piv =  4.0_real64*atan(1.0_real64)
  piv2 =  4.0_real128*atan(1.0_real128)

  write(9,*)pi,  1.0, precision(1.0)
  write(9,*)pil, 1.0_real32, precision(1.0_real32)
  write(9,*)piv, 1.0_real64, precision(1.0_real64)
  write(9,*)piv2,1.0_real128,  precision(1.0_real128)

  write(9,"(f5.3)") pi
  ! write(*,*)pi,  1.0, precision(1.0)
End Program pi_iso
