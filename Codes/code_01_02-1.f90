!File=code_01_02-1.f90
!Author=rudra
!Demonstration of recursive function
!Created=Thu 31 Jan 2019 10:16:02 IST
!Last Modified=Thu 31 Jan 2019 10:16:02 IST
Program permcomb 
  Implicit None
  integer:: n,r,nf, npr,ncr, factorial

  read(12,*) n,r
  npr =  factorial(n)/factorial(n-r)
  ncr =  npr/factorial(r)
  write(*,*) npr, ncr
End Program permcomb


integer recursive function factorial(n) result(fact)
  Implicit None
  integer, intent(in):: n
  
  select case (n)
  case (0:1)
    fact = 1
  case default
    fact =  n* factorial(n-1)
  end select

  ! if (n<=1) then
    ! fact = 1
  ! else
    ! fact = n*factorial(n-1)
  ! End if
end function  factorial
