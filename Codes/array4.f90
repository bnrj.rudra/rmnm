!File=array1.f90
! Demonstrate array operation time based on column-major
!Author=rudra
!Created=Thu 17 Jan 2019 21:06:13 IST
!Last Modified=Thu 17 Jan 2019 21:06:13 IST
Program array1
  Implicit None
  integer,parameter:: n=10000
  integer, dimension(n,n)::a,b
  integer:: i,j
  
  do i = 1, n
    do j =  1, n
      a(i,j) =  i*j
    End do
  End do
End Program array1
