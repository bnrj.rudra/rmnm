!File=array_dynamic.f90
! f2c with dynamic array
!Author=rudra
!Created=Sat 19 Jan 2019 11:29:14 IST
!Last Modified=Sat 19 Jan 2019 11:29:14 IST
Program array_dynamic
  Implicit None
  integer:: n   !number of f temperature to be converted
  integer:: i
  real,dimension(:),allocatable::ftemp

  write(*,*) "input how many numbers you want to convert"
  read(*,*) n

  allocate(ftemp(n))
  ftemp =  (/(i*20, i=1,n)/)

  do i =  1, n
    write(*,*)i, ftemp(i), (ftemp(i)-32)*5/9
  End do

End Program array_dynamic
