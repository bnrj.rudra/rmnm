Program F2C
! Convert Farenheit to Centigrade
implicit none
real(4)::f,c
integer::i
character(len=6)::season 
character(len=20):: op , frmt
op =  "Temperature in C: "
do i =  1,5
  write(*,*)"Input Temp in Fahrenheit", i
  read(*,*) f
  c =  5*(f-32.0)/9.0
  if (c>35) then
    season =   "Summer"
  else if (c < 15) then
    season= "winter"
  else
    season= "Spring"
  end if
  write(*,*) c, season
end do
End Program F2C
