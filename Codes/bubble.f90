!File=bubble.f90
!Author=rudra
!Created=Mon 04 Feb 2019 20:55:40 IST
!Last Modified=Mon 04 Feb 2019 20:55:40 IST
module  mbubble
  Implicit None

contains
  subroutine order(p,q)
    Implicit none
    integer::p,q,temp
    if (p > q) then  ! swap if not ordered
      temp = p
      p = q
      q = temp
    End if
  end subroutine order

  subroutine  bubble(A,n)
    Implicit None
    integer::A(n),i,j,n
    do i=1,n                            ! for all elements, one by one
      do j = n, i+1, - 1                ! search for each pair, starting from the last element
        call order(A(j-1), A(j))
        write(*,"(10(i2,2x))") A
      End do
      write(*,"('After iteration',2x,i2,2x, '=>')", advance="no")i
      write(*,"(10(i2,2x))") A
    End do
    ! write(*,*) "After Sort"
    write(*,"('After Sort', 13x, 10(i2,2x))") A
    ! do i = 1,n
      ! write(12,*)A(i)
    ! end do
  end subroutine  bubble
end module  mbubble


Program pbubble
  use  mbubble
  Implicit None
  integer::i
  integer,parameter:: n=10
  integer:: A(n)

  do i = 1,n
    A(i) = int(RAND()*10)
    ! write(*,*) A(i)
  End do
  write(*,"('Before Sort', 12x, 10(i2,2x))") A

  call bubble(A,n)
End Program pbubble
