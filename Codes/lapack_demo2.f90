!File=lapack_demo2.f90
!Author=rudra
!Created=Fri 08 Mar 2019 09:08:13 IST
!Last Modified=Fri 08 Mar 2019 09:08:13 IST
Program lapack_demo2
  Implicit None
  integer, parameter:: n=5, lwmax = 1000
  integer:: lda = n
  real(8),dimension(n,n):: a
  real:: ad(n*n)

  real(8), dimension(n):: wi, wr, vl,vr, work
  integer::lvdl, lvdr, lwork
  integer::i,j,info
  
  call random_number(ad)
  a = reshape(ad,(/n,n/)) 

  do i=1,n
    write(*,999) (a(i,j),j=1,n)
  End do

  lwork = - 1
  call dgeev("V", "V", n, a, lda, wr, wi, vl, lvdl, vr, lvdr, work, lwork, info)
  write(*,"('Info = ', i2)") info
  if (info == 0) THEN
    write(*,'("Work space size = ", * (f7.2))') work!(1)
    lwork = min(lwmax, int(work(1)))
    call dgeev("N", "N", n, a, lda, wr, wi, vl, lvdl, vr, lvdr, work, lwork, info)
    write(*,999) wr
  end if

999 format(*(f8.5,2x))
End Program lapack_demo2
