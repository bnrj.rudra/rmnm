!File=code_30_1.f90
!Author=rudra
!Created=Wed 30 Jan 2019 07:52:18 IST
!Last Modified=Wed 30 Jan 2019 07:52:18 IST
Program code_30_1
  use mfact
  Implicit None
  integer:: n,r!,nf,rf,nrf,perm,comb,fact
  integer::i

  n = 5
  r = 3

!  nf = 1; rf = 1; nrf=1

  write(*,*) "From Function"
  write(*,*) fact(n)/fact(n-r)
  write(*,*) fact(n)/(fact(r)*fact(n-r))
  write(*,*) fact(3)
End Program code_30_1
