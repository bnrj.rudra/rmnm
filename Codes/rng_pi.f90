!File=rng_pi.f90
!Author=rudra
!Created=Wed 23 Jan 2019 17:44:00 IST
!Last Modified=Wed 23 Jan 2019 17:44:00 IST
Program rng_pi
  use iso_fortran_env
  use omp_lib
  Implicit None
  integer(int64):: i, scount =  0, sample,j,nt
  real(real64):: x,y, cpi,tpi
  tpi =  4*atan(1.0)
  do j =  1,6
    sample = 10**j
    scount =  0
    !$OMP PARALLEL DO
    do i =  1,sample
      x =  rand()
      y =  rand()
      ! write(*,*)x,y
      if (sqrt(x**2+y**2) <= 1.0) scount =  scount+1
    End do
    !$OMP END PARALLEL DO
    cpi =  4*scount/(1.0*sample)
    write(*,*) sample, cpi, (tpi-cpi)/tpi
  end do
End Program rng_pi
