!File=i.f90
! Simple Array function demonstration
!Author=rudra
!Created=Fri 18 Jan 2019 20:08:05 IST
!Last Modified=Fri 18 Jan 2019 20:08:05 IST
Program Ip
  Implicit None
  integer,dimension(3)::a,b
  integer,dimension(3,3)::c
  integer,dimension(1,3)::d
  integer,dimension(0:3,0:3)::e
  integer::i,j
  character,dimension(40)::ch

  ! 1-d matrices
  a =  (/1,2,3/)
  b =  (/(i,i=5,7)/)

  ! 2-d matrices
  d =  reshape((/8,9,10/),(/1,3/))
  c =  reshape((/(i, i=1,9)/),(/3,3/))
  
  do i = 1,40
    ch(i) =  "="
  End do
  write(*,*) "Mat A"
  write(*,"(A6, 2x, I2, 4x, A6, I2, 4x, A6, 2x, 3(I2,2x))") &
          "size:",size(a), "shape:",shape(a), "value:",a
  write(*,"(40(A))")ch
  write(*,*) "Mat B"
  write(*,"(A6, 2x, I2, 4x, A6, I2, 4x, A6, 2x, 3(I2,2x))") &
    "size:",size(b), "shape:",shape(b), "value",b
  write(*,"(40(A))")ch
  write(*,*) "Mat D"
  write(*,"(A6, 2x, I2, 4x, A6, I2, I2, 4x, A6, 2x, 3(I2,2x))") &
    "size:",size(d), "shape:",shape(d), "value",d
  write(*,"(40(A))")ch
  write(*,"(A6,2x,A6,2x,I2,4x, A6,2x,I2, 1x,I2)") "Mat E", "size:",size(e), "shape:",shape(e)
  write(*,"(40(A))")ch
  write(*,"(A6,2x,A6,2x,I2,4x, A6,2x,I2, 1x,I2)") "Mat C", "size:",size(c), "shape:",shape(c)
  do i =  1,3
   write(*,"(3(I3,2x))") (c(i,j),j=1,3)
  End do
  write(*,*)"row1, col2 =>", c(1,2)
  write(*,"(A14,3(I3,2x))")"row1 =>", c(1,:)
  write(*,"(A14,3(I3,2x))")"col2 =>", c(:,2)
  write(*,"(40(A))")ch
  
  write(*,"('DOT PRODUCT(A,B):', I3)")dot_product(a,b)
  write(*,"('DOT PRODUCT(B,A):', I3)")dot_product(b,a)
  write(*,"('MATRIX MULTIPLICATION(C,A):', 3(I3,2x))")matmul(c,a)
  write(*,"('MATRIX MULTIPLICATION(A,C):', 3(I3,2x))")matmul(a,c)
  write(*,"('MATRIX MULTIPLICATION(D,C):', 3(I3,2x))")matmul(d,c)
End Program Ip
