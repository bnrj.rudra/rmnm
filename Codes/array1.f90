!File=f2c_arr.f90
!Author=rudra
!Created=Fri 18 Jan 2019 15:41:02 IST
!Last Modified=Fri 18 Jan 2019 15:41:02 IST
Program f2c_arr
  Implicit None
  real, dimension(5)::ftemp 
  real, dimension(2,5)::ctemp
  character(len=6), dimension(3)::season 
  integer::i,j
  real::c

  ftemp =  [20, 40, 60, 80, 100]
  season =  [ "Summer", "Winter", "Spring" ]

  write(*,*)"Shape of ftemp", shape(ftemp)
  write(*,*)"size of ftemp",  size(ftemp)

  do i =  1, size(ftemp) 
    c =  (ftemp(i) - 32)*5/9
    ctemp(1,i) =  c
    ctemp(2,i) =  2*c
  End do

  ! write(*,*) ftemp
  ! write(*,*) "Ctemp", ctemp
  ! write(*,*) "ctemp 1st row"
  ! write(*,*) ctemp(1,:)
  ! write(*,*) "ctemp 2nd row"
  ! write(*,*) ctemp(2,:)

  write(*,*) "ctemp using do loop"
  do i =  1, 2
    write(*,"(5(f6.2, 2x))") (ctemp(i,j), j=1,5)
  End do
  write(*,*)"Shape of ctemp", shape(ctemp)
  write(*,*)"Size of ctemp ",  size(ctemp)

End Program f2c_arr
