!File=sine.f90
!Author=rudra
!Created=Wed 30 Jan 2019 10:44:00 IST
!Last Modified=Wed 30 Jan 2019 10:44:00 IST
Program sine
  Implicit None
  real(8):: sinx, x, tsinx, error
  integer::n,i,fact,counter
  real(4)::er
  character(len=80):: comment, comment1, comment2 
  x=atan(1.0)             !\pi/4
  error = epsilon(er)
  write(*,*)error
  write(*,"('Loop', 2x, 3(A10,2x), a20)")"Sin series", "Sin func", "Difference", adjustl("Comment")
  comment1 = "Not converged, still running"
  comment2 = 'Already converged, doing unnecessary calculation'
  sinx = 0
  counter = 0
  tsinx = sin(x)
  do !i = 0,10
    i = counter
    comment = comment1
    sinx = sinx + (((-1)**i)*x**((2*i)+1))/fact((2*i)+1)
    if (abs(tsinx - sinx)<=error) then
      comment = comment2
      exit
    End if
    write(*,"(i2, 3x, 2(f10.7, 2x), es11.5, 2x, a40)") counter, sinx, sin(x), abs(tsinx-sinx), comment
    counter = counter+1
    ! write(*,*)
  End do
End Program sine

integer function fact(n)
  integer::n,i
  fact=1
  do i = 1,n
    fact = fact*i
  End do
end function fact
