!File = pi.f90
!Author = rudra
!Created = Fri11Jan201909:02:46IST
!LastModified = Fri11Jan201909:02:46IST
Program getpi
Implicit None
  integer,parameter::vlong = selected_real_kind(16,1)
  integer,parameter::long = selected_real_kind(8,1)
  real::pi
  real(long)::pil
  real(vlong)::piv,piv2
  !Define\pias4*arctan(1)

  pi = 4.0*atan(1.0)
  pil = 4.0_long*atan(1.0_long)
  piv = 4.0_long*atan(1.0_long)
  piv2 = 4.0_vlong*atan(1.0_vlong)

  write(*,*)pi,precision(1.0)
  write(*,*)pil,precision(1.0_long)
  write(*,*)piv,precision(1.0_vlong)
  write(*,*)piv2,precision(1.0_vlong)

  write(*,"('Value of Pi is :', f8.5, 2x, f20.18)")pi, pi
  write(*,"('Value of Pi is :',f8.5, 2x, f20.18)")pil,pil
  write(*,"('Value of Pi is :',f8.5, 2x, f20.18)")piv,piv
  write(*,"('Value of Pi is :',f8.5, 2x, f20.18)")piv2, piv2
  
  write(*,"('Value of Pi is :',f8.5, 2x, f20.20)")piv2, piv2

End Program getpi
