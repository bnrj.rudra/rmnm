!File=range.f90
!Author=rudra
!Created=Thu 10 Jan 2019 14:47:16 IST
!Last Modified=Thu 10 Jan 2019 14:47:16 IST
Program rangex
  Implicit None
  integer, parameter :: r_two = selected_real_kind(4,1)
  integer, parameter :: r_four = selected_real_kind(8,1)
  integer, parameter :: r_eight = selected_real_kind(16,1)
  
  integer, parameter :: i_two = selected_int_kind(4)
  integer, parameter :: i_four = selected_int_kind(8)
  integer, parameter :: i_eight = selected_int_kind(16)
  
  real(kind = r_four):: r
  ! integer::
  write(*,*)"Integer========== "
  write(*,*) "Default    :  Type:",   kind(1)        ,  "Range:",   range(1)             
  write(*,*) "Two Bytes  :  Type:",   kind(1_i_two)  ,  "Range:",   range(1_i_two)   
  write(*,*) "Four Bytes :  Type:",   kind(1_i_four) ,  "Range:",   range(1_i_four)  
  write(*,*) "Eight Bytes:  Type:",   kind(1_i_eight),  "Range:",   range(1_i_eight) 
  write(*,*) range(r)
  ! real::
  write(*,*)"Real   ========== "
  write(*,*) "Default    :  Type:",   kind(1.0)        ,  "Range:",   range(1.0), &
    "Precision: ", precision(1.0)
  write(*,*) "Two Bytes  :  Type:",   kind(1.0_r_two)  ,  "Range:",   range(1.0_r_two),  & 
    "Precision: ", precision(1.0_r_two)
  write(*,*) "Four Bytes :  Type:",   kind(1.0_r_four) ,  "Range:",   range(1.0_r_four), &  
    "Precision: ", precision(1.0_r_four)
  write(*,*) "Eight Bytes:  Type:",   kind(1.0_r_eight),  "Range:",   range(1.0_r_eight), &
    "Precision: ", precision(1.0_r_eight)
End Program rangex
