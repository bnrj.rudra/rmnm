
!File=fibonacci.f90
!Author=rudra
!Created=Tue 22 Jan 2019 18:59:54 IST
!Last Modified=Tue 22 Jan 2019 18:59:54 IST
Program fibonacci
  use iso_fortran_env
  Implicit None
  integer(int64):: n        ! dimension
  integer(int64)::i
  integer(int64), dimension(:), allocatable:: fibbo

  write(*,*) "Enter size of fibonacci list"
  read(*,*) n

  allocate(fibbo(0:n))
  fibbo(0) =  1
  fibbo(1) =  1

  do i =  2, n
    fibbo(i) =  fibbo(i-2)+fibbo(i-1)
  End do

  do i =  0,n
    write(*,*) fibbo(i)
  End do
End Program fibonacci
