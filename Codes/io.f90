!File=io.f90
!Author=rudra
!Created=Fri 08 Feb 2019 08:42:31 IST
!Last Modified=Fri 08 Feb 2019 08:42:31 IST
Program io
  Implicit None

  ! define individual variables
  integer:: i,k
  real(4):: re
  character(len=10)::pn
 
  ! user defined data type
  type:: person
    integer:: goal,assist
    real(4):: minute
    character(len=10)::pname
  end type person
  type(person):: smo

  type, extends(person)::pex
    integer,dimension(4)::goaly
  end type pex
  type(pex):: smo2

  namelist /spec/i,k,re,pn 

  ! READ SELECTED VALUE FROM FILE
  open(14, file="test.dat")
  read(14,*)i,k,re
  close(14)
  write(*,*)i,k, re

  ! READ SELECTED VALUE FROM FILE
  open(14, file="test.dat")
  read(14,*)i,k,re
  close(14)
  write(*,*)i,k, re, pn

  ! READ FROM FILE 
  open(14, file="test.dat")
  read(14,*) smo
  close(14)
  write(*,*)"SMO", smo
  
  ! READ FROM FILE
  open(14, file="test.dat")
  read(14,*) smo2
  close(14)
  write(*,*)"SMO2", smo2


  ! READ FROM NAMELIST
  open(8,file="specs.in")
   read(8,NML=spec)
  close(8)
  write(*,*)i,k,re,pn
End Program io
