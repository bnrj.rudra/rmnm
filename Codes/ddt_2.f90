!File=ddt_2.f90
!Author=rudra
!Created=Fri 25 Jan 2019 08:20:49 IST
!Last Modified=Fri 25 Jan 2019 08:20:49 IST
Program ddt_2
  Implicit None
  type:: vector
    real(4)::a,b,c
  end type vector

  type(vector):: va,vb,vc

  va = vector(2,2,5)
  vb = vector(3.0,7,2)


  vc =  vector(va%a+vb%a, va%b+vb%b, 0)
  write(*,*) vc
  vc%c =  1.23
  write(*,*) vc
End Program ddt_2
