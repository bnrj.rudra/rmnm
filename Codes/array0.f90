Program F2C
! Convert Farenheit to Centigrade
implicit none
real(4)::f,c
integer::i
character(len=6), dimension(3)::season 
character(len=20):: op , frmt
season =  [ "Summer", "Winter", "Spring" ]
op =  "Temperature in C: "
do i =  1,5
  write(*,*)"Input Temp in Fahrenheit", i
  read(*,*) f
  c =  5*(f-32.0)/9.0
  if (c>35) then
    frmt =  season( 1 )
  else if (c < 15) then
    frmt= season(2)
  else
    frmt =  season(3)
  end if
  write(*,*)op, c, frmt
end do
End Program F2C
