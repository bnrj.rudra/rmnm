!File=ddt_1.f90
!Author=rudra
!Demo code for derived data type
!Created=Thu 24 Jan 2019 22:52:12 IST
!Last Modified=Thu 24 Jan 2019 22:52:12 IST
Program ddt_1
  Implicit None
  integer(8)::i
  integer,parameter::s =  2
  type::person
    character(len=10)::first_name, last_name
    character(len=20)::email
    character(len=10):: assignments
    logical:: done
  end type person

  type(person):: debraj, sachin, suneel
  type(person), dimension(s):: new_person
  debraj =  person('debraj', 'bose', 'db@hri','fibbo', .true.)
  suneel =  person('suneel', 'vemula', 'vs@hri','fibbo', .true.)


  write(*,*) debraj%done, debraj%assignments 
  write(*,"(A4, 7x, A8, 2x, A6, 2x, A10, 2x, A10)") &
           "Name", "Surname" , "Email", "Assignment", "Status"
  write(*,"(A10, 2x, A10, 2x, A6, 2x, A10, 2x, L2)") suneel
  write(*,"(A10, 2x, A10, 2x, A6, 2x, A10, 2x, L2)") debraj

  pause
  do i=1,s
    write(*,"(A10,2x,I2)") "Add Person ", i
    read(*,*) new_person(i)
  End do
  pause
  write(*,*) "Complete data"
  do i = 1,s
    write(*,*)new_person(i)
  End do

  pause
  write(*,*)"One component of the data"
  do i = 1,s
    write(*,*) new_person(i)%email
  End do
End Program ddt_1
