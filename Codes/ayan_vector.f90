!AYAN NANDA,MSC,HRI
!ARITHMETIC OPERATION OF VECTORS
PROGRAM vector_addition_subtraction_multiplication
IMPLICIT NONE
TYPE::VECTOR
REAL::i,j,k
END TYPE VECTOR
TYPE(VECTOR)::Va,Vb,Vc,Vd,Ve
Va=VECTOR(2,10,12)
Vb=VECTOR(8,3,1)
Vc=Vector(Va%i+Vb%i,Va%j+Vb%j,Va%k+Vb%k)
Vd=Vector(Va%i-Vb%i,Va%j-Vb%j,Va%k-Vb%k)
Ve=Vector(Va%i*Vb%i,Va%j*Vb%j,Va%k*Vb%k)
write(*,*)"Vc=",Vc      !Addition
write(*,*)"Vd=",Vd      !substraction
write(*,*)"Ve=",Ve       !multiplication
end program vector_addition_subtraction_multiplication
