!File=precision.f90
!Author=rudra
! A code to demonstrate how precision works
!Created=Mon 28 Jan 2019 15:21:41 IST
!Last Modified=Mon 28 Jan 2019 15:21:41 IST
Program precision_test
  use iso_fortran_env
  Implicit None
  integer, parameter :: r2=selected_real_kind(4,2)
  integer, parameter :: r4=selected_real_kind(8,2)
  integer, parameter :: r8=selected_real_kind(16,2)

  integer, parameter :: i2=selected_int_kind(2)
  integer, parameter :: i4=selected_int_kind(4)
  integer, parameter :: i5=selected_int_kind(3)
  integer, parameter :: i8=selected_int_kind(8)
  integer, parameter :: i16=selected_int_kind(16)

  integer(i2):: inti2
  integer(i4):: inti4
  integer(i5):: inti5
  integer(i8):: inti8
  integer(i16):: inti16

  integer::in0
  integer(1)::in1
  integer(2)::in2
  integer(4)::in4
  ! integer(3)::in5
  integer(8)::in8

  real(r2)::pik2
  real(r4)::pik4
  real(r8)::pik8
  !---------
  real(real32)::pir2
  real(real64)::pir4
  real(real128)::pir8,pir128

  ! About integer
  inti2 =  2000
  inti4 =  2000
  inti5 =  4000
  inti8 =  2000
  inti16=  2000
  in0 =  2000
  in1 =  2000
  in2 =  2000
  in4 =  2000
  ! in5 =  2000
  in8 =  2000
!   write(*,1001)"inti2", inti2,   huge(inti2), kind(inti2)
!   write(*,1001)"inti4", inti4,   huge(inti4), kind(inti4)
!   write(*,1001)"inti3", inti5,   huge(inti5), kind(inti5)
!   write(*,1001)"inti8", inti8,   huge(inti8), kind(inti8)
!   write(*,1001)"inti16",inti16,  huge(inti16), kind(inti16)
!   write(*,1001)"in0",in0,  huge(in0), kind(in0)
!   write(*,1001)"in1",in1,  huge(in1), kind(in1)
!   write(*,1001)"in2",in2,  huge(in2), kind(in2)
!   write(*,1001)"in4",in4,  huge(in4), kind(in4)
!   ! write(*,1001)"in5",in5,  huge(in5), kind(in5)
!   write(*,1001)"in8",in8,  huge(in8), kind(in8)

  ! write(*,*)"inti2", inti2,   huge(inti2), kind(inti2)
  ! write(*,*)"inti4", inti4,   huge(inti4), kind(inti4)
  ! write(*,*)"inti8", inti8,   huge(inti8), kind(inti8)
  ! write(*,*)"inti16",inti16,  huge(inti16), kind(inti16)

  ! pause
  !About Real
  pik2=4.0*atan(1.0)
  pik4=4.0*atan(1.0)
  pik8=4.0*atan(1.0)
  pir2=4.0*atan(1.0)
  pir4=4.0*atan(1.0)
  pir8=4.0*atan(1.0)

  write(*,*) "We are asking by length"
  write(*,1002)"pik2",pik2, precision(pik2), huge(pik2), kind(pik2)
  write(*,1002)"pik4",pik4, precision(pik4), huge(pik4), kind(pik4)
  write(*,1002)"pik8",pik8, precision(pik8), huge(pik8), kind(pik8)
  write(*,*) "We are asking by bit"
  write(*,1002)"pir2",pir2, precision(pir2), huge(pir2), kind(pir2)
  write(*,1002)"pir4",pir4, precision(pir4), huge(pir4), kind(pir4)
  write(*,1002)"pir8",pir8, precision(pir8), huge(pir8), kind(pir8)
  1001 format(A6, 2x, i6, 4x, "Max:",2x, i20, 2x, "kind:",2x, i2)
  1002 format(A6, 2x, f8.6, 2x, "Kind:",2x, i2, 2x, "Max:",2x, e22.12, "Kind:", 2x, i4)
End Program precision_test
