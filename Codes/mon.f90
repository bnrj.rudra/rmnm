!File=mon.f90
!Author=rudra
!Demo for month and season number from an integer
!Created=Wed 23 Jan 2019 03:22:27 IST
!Last Modified=Wed 23 Jan 2019 03:22:27 IST
Program mon
  Implicit None
  integer:: i,n
  character(len=3), dimension(8):: month
  character(len=3),dimension(4):: season
  month=(/"Jan", "Feb", "Mar", "Apr", "May","Jun", "Jul", "Aug"/)
  season=(/"Win","Spr","Sum","Rai"/)

  write(*,*)"Input month as integer"
  read(*,*)n

  write(*,"('Month is', 2x, A3,'; Season is',2x, A3)")  month(n),  season(ceiling(n/2.))
End Program mon
