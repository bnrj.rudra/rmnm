module vec_arth
  implicit none
  type::vector
    real::i,j,k
  end type vector
contains
  type(vector) function vadd(va,vb)
    type(vector)::Va,Vb
    vadd = Vector(Va%i+Vb%i,Va%j+Vb%j,Va%k+Vb%k)
  End function vadd
  type(vector) function vsub(va,vb)
    type(vector)::Va,Vb
    vsub = Vector(Va%i-Vb%i,Va%j-Vb%j,Va%k-Vb%k)
  End function vsub
  type(vector) function vmult(va,vb)
    type(vector)::Va,Vb
    vmult= Vector(Va%i*Vb%i,Va%j*Vb%j,Va%k*Vb%k)
  End function vmult
end module
